;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; satwrap.asd --- SAT solver wrapper for CL

;; Copyright (C) 2010 Utz-Uwe Haus <lisp@uuhaus.de>
;;
;; $Id:$
;;
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 3 of the GNU General
;; Public License as published by the Free Software Foundation, as
;; clarified by the prequel found in LICENSE.Lisp-GPL-Preface.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 3 of the GNU General Public License is in the file
;; LICENSE.GPL that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/gpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; 

;;; Code:

#-cffi
(progn
  #+quicklisp
  (ql:quickload "cffi")
  #-quicklisp
  (asdf:operate 'asdf:load-op "cffi"))

;; my old asdf-component-shared-unix-library-system.asd, mostly
(defclass shared-unix-library (module)
  ((library-designator
    :type (or symbol string list pathname)
    :initarg :library-designator
    :documentation
    "Library designator suitable to be passed to cffi:load-foreign-library")))

;; (defmethod source-file-type ((component shared-unix-library) system)
;;   "so")
(defmethod input-files (operation (component shared-unix-library))
  nil)
(defmethod output-files ((operation compile-op) (component shared-unix-library))
  nil)
(defmethod perform ((operation compile-op) (component shared-unix-library))
  nil)
(defmethod operation-done-p ((o compile-op) (c shared-unix-library))
  t)
(defmethod operation-done-p ((o load-op) (c shared-unix-library))
  nil)
(defmethod perform ((operation load-op) (component shared-unix-library))
  (let ((cffi:*foreign-library-directories*
	 ;; add autoconf substitution here:
	 `(#p"/Users/uhaus/work/cray/eiger-cpp/.libs/"
	     #p"/usr/local/lib/" ;; might work...
	     ,cffi:*foreign-library-directories*)))
    (let ((lib
	   (cffi:load-foreign-library
	    (slot-value component 'library-designator))))
      (format *standard-output*
	      "~&;; loaded shared-unix-library ~A as ~A~%"
	      (component-name component)
	      lib)
      lib)))

(defsystem #:satwrap
    :description "?"
    :version     "0"
    :author      "Utz-Uwe Haus <lisp@uuhaus.de>"
    :license     "ask me"
    :depends-on  ("cffi"
		  "trivial-garbage")
    :components  ((:file "package")
		  (:file "backend" :depends-on ("package"))
		  (:module :backends
			   :components
			   ((:module
			     :precosat
			     :serial T
			     :components ((:shared-unix-library 
					   "libprecosat"
					   :library-designator
					   (:or
					    "libprecosat"
					    "libprecosat_so"
					    "libprecosat.so"
					    "libprecosat.dylib"))
					  (:file "satwrap.precosat")))
			    (:module
			     :minisat
			     :serial T
			     :components ((:shared-unix-library 
					   "libminisat"
					   :library-designator
					   (:or
					    "simp/.libs/libminisat"
					    "simp/.libs/libminisat.so"
					    "libminisat_so"
					    "libminisat.so"))
					  (:file "satwrap.minisat"))))
			   :depends-on ("backend" "package"))
		  ;; top-level:

		  (:file "satwrap" :depends-on ("package" 
						"backends"))
		  (:file "dimacs" :depends-on ("package" "backend" "satwrap"))
		  ))

