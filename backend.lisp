;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; backend.lisp --- SAT wrapper generic backend definition

;; Copyright (C) 2010 Utz-Uwe Haus <lisp@uuhaus.de>
;; $Id:$
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 3 of the GNU General
;; Public License as published by the Free Software Foundation, as
;; clarified by the prequel found in LICENSE.Lisp-GPL-Preface.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 3 of the GNU General Public License is in the file
;; LICENSE.GPL that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/gpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; 

;;; Code:


(eval-when (:compile-toplevel :load-toplevel)
  (declaim (optimize (speed 3) (debug 1) (safety 1))))

(in-package #:satwrap.backend)

;;; Backend:
(defclass satwrap-backend ()
  ((name :accessor sat-backend-name :initarg :name)
   (version :accessor sat-backend-version :initarg :version))
  (:documentation "Superclass of all sat backends"))


;; these generics need implementations for each backend:
(defgeneric (setf numvars) (numvars backend)
  (:documentation "Declare number of variables used in backend."))

(defgeneric add-clause (backend clause)
  (:documentation "Add CLAUSE to CNF of BACKEND."))

(defgeneric satisfiablep (backend assumptions)
  (:documentation "Check BACKEND for satisfiability under ASSUMPTIONS. 
Returns T or NIL.")
  ;; no default implementation, but convert vector to list for dumb backends
  (:method ((be satwrap-backend) (assumptions vector))
    (satisfiablep be (coerce assumptions 'list))))

(defgeneric solution (backend interesting-vars)
  (:documentation "Return the values of INTERESTING-VARS in last SAT solution of BACKEND."))


;; these generics do not need to be implemented as there are default implementations
(defgeneric get-essential-variables (backend)
  (:documentation "Compute the variables that are essential, i.e. fixed in all solutions of SOLVER. 
Returns a list of literals fixed, sign according to phase."))

(defgeneric synchronize-backend (backend numvars new-clauses deleted-clauses old-clauses)
  (:documentation "Populate CNF with NUMVARS variables in BACKEND. 
NEW-CLAUSES are the clauses added since the last call to synchronize-backend,
DELETED-CLAUSES are the clauses deleted since the last call to synchronize-backend,
OLD-CLAUSES is the union of the NEW-CLAUSES and the OLD-CLAUSES minus the DELETED-CLAUSES when synchronize-backend was last called, including the DELETED-CLAUSES.")
  (:method ((backend satwrap-backend) numvars 
	    (new-clauses list)
	    (deleted-clauses list)
	    (old-clauses list))
    "Default implementation: Stuff everything into a possibly fresh solver."
    (reinitialize-instance backend)
    ;; declare proper number of variables
    (setf (numvars backend) numvars)
    ;; old clauses, including deletion
    (loop :for c :in old-clauses
       :unless (member c deleted-clauses :test #'equal)
       :do (add-clause backend c))
    ;; new clauses
    (dolist (c new-clauses)
      (add-clause backend c))))


