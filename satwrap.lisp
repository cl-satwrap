;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; satwrap.lisp --- SAT solvers wrapped for CL

;; Copyright (C) 2010 Utz-Uwe Haus <lisp@uuhaus.de>
;; $Id:$
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 3 of the GNU General
;; Public License as published by the Free Software Foundation, as
;; clarified by the prequel found in LICENSE.Lisp-GPL-Preface.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 3 of the GNU General Public License is in the file
;; LICENSE.GPL that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/gpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; 

;;; Code:

(eval-when (:compile-toplevel :load-toplevel)
  (declaim (optimize (speed 3) (debug 1) (safety 1))))


(in-package #:satwrap)

(defvar *default-sat-backend* :minisat
  "Name of the default backend to be used by make-sat-solver.")

(defparameter *satwrap-backends* 
  `((:precosat . ,(find-class 'satwrap.precosat:precosat-backend))
    (:minisat  . ,(find-class 'satwrap.minisat:minisat-backend)))
  "Alist of symbolic name to backend class name for all supported backends.")

(defun describe-supported-backends ()
  "Return a list of (designator . description) pairs of the supported backends"
  (loop :for (key . class) :in *satwrap-backends*
     :as descr := (let ((instance (make-instance class)))
		    (format nil "~A version ~A"
			    (satwrap.backend:sat-backend-name instance)
			    (satwrap.backend:sat-backend-version instance)))
     :collect `(,key . ,descr)))


;;; Frontend:
(defclass sat-solver ()
  ((backend :initarg :backend :accessor sat-solver-backend)
   (numvars :initform 0 :accessor sat-solver-numvars)
   ;; CNF; split into 'old' and 'new' to allow incremental solving
   (new-clauses :initform '() :accessor sat-solver-new-clauses)
   (old-clauses :initform '() :accessor sat-solver-old-clauses) 
   )
  (:default-initargs :backend (make-instance (cdr (assoc *default-sat-backend*
							 *satwrap-backends*))))
  (:documentation "A Sat solver abstraction"))

(defmethod sat-solver-numclauses ((solver sat-solver))
  (+ (length (sat-solver-new-clauses solver))
     (length (sat-solver-old-clauses solver))
     ))

(defmethod print-object ((solver sat-solver) stream)
  (print-unreadable-object (solver stream :type T :identity T)
    (format stream "~D vars, ~D clauses, backend ~A" 
	    (sat-solver-numvars solver)
	    (sat-solver-numclauses solver)
	    (sat-solver-backend solver))))

(defmethod clause-valid ((solver sat-solver) (clause list))
  "Check that CLAUSE is a valid clause for SOLVER."
  (loop
     :with max := (sat-solver-numvars solver)
     :with min := (- max)
     :for v :in clause
     :always (and (not (zerop v))
		  (<= min v max))))

(defmethod clause-valid ((solver sat-solver) (clause vector))
  "Check that CLAUSE is a valid clause for SOLVER."
  (loop
     :with max := (sat-solver-numvars solver)
     :with min := (- max)
     :for v :across clause
     :always (and (not (zerop v))
		  (<= min v max))))

(defmacro iota (n &optional (start 1))
  "Return a list of the N sequential integers starting at START (default: 1)."
  (let ((i (gensym "i")))
    `(loop :repeat ,n
	:for ,i :from ,start
	:collect ,i)))

(define-condition satwrap-condition (error) 
  ()
  (:documentation "Superclass of all conditions raised by satwrap code."))

(define-condition invalid-clause (satwrap-condition)
  ((solver :initarg :solver :reader invalid-clause-solver)
   (clause :initarg :clause :reader invalid-clause-clause))
  (:report (lambda (condition stream)
	     (format stream "Invalid clause ~A for solver ~A" 
		     (invalid-clause-clause condition)
		     (invalid-clause-solver condition)))))

(define-condition invalid-backend (satwrap-condition)
  ((name :initarg :name :reader invalid-backend-name))
  (:report (lambda (condition stream)
	     (declare (special *satwrap-backends* *default-sat-backend*))
	     (format stream "Invalid backend ~S specified. Supported: ~{~S~^, ~}, default ~S."
		     (invalid-backend-name condition)
		     (mapcar #'car *satwrap-backends*)
		     *default-sat-backend*))))

(defmethod flush-to-backend ((solver sat-solver))
  "Populate backend, possibly flushing old backend contents."
  (let ((deleted '()))
    (satwrap.backend:synchronize-backend (sat-solver-backend solver)
					 (sat-solver-numvars solver)
					 (sat-solver-new-clauses solver)
					 deleted
					 (sat-solver-old-clauses solver))
    (setf (sat-solver-old-clauses solver) 
	  (nconc (sat-solver-new-clauses solver)
		 (if deleted
		     (delete-if #'(lambda (c)
				    (find c deleted :test #'equal))
				(sat-solver-old-clauses solver))
		     (sat-solver-old-clauses solver))))
    (setf (sat-solver-new-clauses solver) '())))


(defun make-sat-solver (&optional (backend *default-sat-backend*))
  "Return a new sat solver instance. Optional argument BACKEND can be used to
specify which backend should be used. It defaults to *default-sat-backend*."
  (let ((be (assoc backend *satwrap-backends* :test #'eq)))
    (if be
	(make-instance 'sat-solver :backend (make-instance (cdr be)))
	(error 'invalid-backend :name backend))))

(defgeneric add-variable (solver)
  (:documentation "Add another variable to SOLVER. Returns new variable index.")
  (:method ((solver sat-solver))
    (incf (sat-solver-numvars solver))))

(defgeneric add-clause (solver clause)
  (:documentation "Add CLAUSE to SOLVER's cnf formula. Clause is consumed.")
  (:method ((solver sat-solver) (clause list))
    (if (clause-valid solver clause)
	(push clause (sat-solver-new-clauses solver))
	(error 'invalid-clause :clause clause :solver solver)))
  (:method ((solver sat-solver) (clause vector))
    (add-clause solver (coerce clause 'list))))

(defgeneric add-clauses (solver clauses)
  (:documentation "Add CLAUSES to SOLVER's cnf formula. Clauses are consumed.")
  (:method ((solver sat-solver) (clauses list))
    (dolist (c clauses)
      (add-clause solver c))))

(defgeneric satisfiablep (solver &key assume)
  (:documentation "Check whether current CNF in SOLVER is satisfiable. 
Keyword argument :ASSUME can provide a sequence of literals assumed TRUE or FALSE.
Returns T or NIL.")
  (:method ((solver sat-solver) &key (assume '()))
    (if (clause-valid solver assume) ;; misusing 'clause' concept here
	(progn
	  (flush-to-backend solver)
	  (satwrap.backend:satisfiablep (sat-solver-backend solver) assume))
	(error 'invalid-clause :clause assume :solver solver))))

(defgeneric solution (solver &key interesting-vars)
  (:documentation "Return solution for SOLVER. If unsat, return '(). If sat return 
sequence of 0/1 values for variables [1...N]. Keyword argument INTERESTING-VARS can be used to restrict the variables whose values are reported. Solution components will be in the same order that INTERESTING-VARS lists the variables in.")
  (:method ((solver sat-solver) 
	    &key (interesting-vars (iota (sat-solver-numvars solver))))
    (satwrap.backend:solution (sat-solver-backend solver) interesting-vars)))

(defgeneric get-essential-variables (solver)
  (:documentation "Compute the variables that are essential, i.e. fixed in all solutions of SOLVER. 
Returns a list of literals fixed, sign according to phase.")
  (:method ((solver sat-solver))
    ;; The backend may define a specialized method, but then it has to
    ;; work on the one copy of data flushed to the solver. Precosat for example
    ;; does not allow that, so we define the universal implementation here instead of
    ;; a default implementation in the backend.
    (let ((have-specialized-method 
	   (find-method #'satwrap.backend:get-essential-variables
			'()
			`(,(class-of (sat-solver-backend solver)))
			nil)))
      (if have-specialized-method
	  (progn
	    (flush-to-backend solver)
	    (satwrap.backend:get-essential-variables (sat-solver-backend solver)))
	  (let ((fixed '()))
	    (loop :for var :of-type (integer 1) :in (iota (sat-solver-numvars solver))
	       :as sat-for-+ := (satisfiablep solver :assume `(,var ,@fixed ))
	       :as sat-for-- := (satisfiablep solver :assume `(,(- var) ,@fixed))
	       :do (cond
		     ((and sat-for-+ (not sat-for--)) 
		      (push var fixed))
		     ((and (not sat-for-+) sat-for--)
		      (push (- var) fixed))))
	    fixed)))))


(defmacro with-sat-solver ((name numatoms &key (backend *default-sat-backend* backend-given))
			   &body body)
  "Bind NAME to a fresh sat-solver with backend :BACKEND (default: *default-sat-backend*) and declare NUMATOMS variables numbered 1..NUMATOMS for the duration of BODY."
  `(let ((,name (make-sat-solver ,(if backend-given 
				      backend
				      '*default-sat-backend*))))
     (loop :repeat ,numatoms :do (add-variable ,name))
     (locally
	 ,@body)))

(defmacro with-index-hash ((mapping &key (test '#'eq)) objects &body body)
  "Execute BODY while binding MAPPING to a hash-table (with predicate
TEST, defaults to #'cl:eq) mapping the elements of the sequence OBJECTS
to integer 1..[length objects].
Typically used to map objects to variables inside WITH-SAT-SOLVER."
  (let ((o (gensym "objects")))
    `(let* ((,o ,objects)
	    (,mapping (etypecase ,o
		       (list (loop :with ht := (make-hash-table :test ,test)
				:for x :in ,o
				:for num :of-type fixnum :from 1
				:do (setf (gethash x ht) num)
				:finally (return ht)))
		       (vector (loop :with ht := (make-hash-table :test ,test)
				  :for x :across ,o
				  :for num :of-type fixnum :from 1
				  :do (setf (gethash x ht) num)
				  :finally (return ht))))))
       ,@body)))

;; convenience syntax for logical operations:
(defun standardize (tree)
  "Convert tree of logical operations to elementary :NOT, :AND, :OR tree (preserving :ATOM).
Supports :IMPLY, :IFF, binary :XOR, and :NOR. "
  (cond
    ((consp tree)
     (destructuring-bind (op . expr)
	 tree
       (case op
	 ((:IMPLY :IMPLIES)
	  (if (= 2 (length expr))
	      `(:OR (:NOT ,(standardize (car expr)))
		    ,(standardize (cadr expr)))
	      (error "non-binary :IMPLY operation: ~A" tree)))
	 (:IFF
	  (if (= 2 (length expr))
	      `(:AND ,(standardize `(:IMPLIES ,(car expr) ,(cadr expr)))
		     ,(standardize `(:IMPLIES ,(cadr expr) ,(car expr))))
	      (error "non-binary :IFF operation: ~A" tree)))
	 (:XOR
	  (if (= 2 (length expr))
	      (let ((e1 (standardize (first expr)))
		    (e2 (standardize (second expr))))
		`(:OR (:AND ,e1 (:NOT ,e2))
		      (:AND ,e2 (:NOT ,e1))))
	      (error "non-binary :XOR operation: ~A" tree)))
	 (:NOR
	  `(:AND ,@(mapcar #'(lambda (p) `(:NOT ,(standardize p))) expr)))
	 
	 (T `(,op ,@(mapcar #'standardize expr))))))
    (T tree)))

(defun standard-tree->nnf (tree)
  "Move NOTs inward. Input must be AND/OR/NOT-only; returns NNF tree."
  (cond
    ((consp tree)
     (destructuring-bind (op . expr)
	 tree
       (case op
	 (:NOT (if (= 1 (length expr))
		   (let ((subexpr (first expr)))
		     (if (consp subexpr)
			 (case (first subexpr)
			   ;; double-not: drop 2 levels
			   (:NOT (standard-tree->nnf (cadr subexpr)))
			   ;; NOT AND: 
			   (:AND `(:OR ,@(mapcar #'(lambda (c)
						     (standard-tree->nnf `(:NOT ,c)))
						 (cdr subexpr))))
			   ;; NOT OR:
			   (:OR  `(:AND ,@(mapcar #'(lambda (c)
						      (standard-tree->nnf `(:NOT ,c)))
						  (cdr subexpr))))
			   ;; `quoted' atom: keep
			   (:ATOM `(:NOT ,subexpr))
			   (otherwise `(:NOT ,subexpr)))
			 ;; non-quoted atom:
			 `(:NOT ,subexpr)))
		   (error "non-unary :NOT expression: ~W" tree)))
	 ;; other operators: AND, OR, ATOM: descend
	 (T
	  `(,op ,@(mapcar #'standard-tree->nnf expr))))))
    ;; non-cons: stop
    (T tree)))

(defun crossprod (list-of-lists)
  "Return a list of all lists containing one element from each sublist of LIST-OF-LISTS."
  (reduce #'(lambda (collected term)
	      (loop :for x :in term :appending
		 (loop :for tail :in collected
		    :collect `(,x ,@tail))))
	  list-of-lists
	  :initial-value '(())))

(defun is-cnf (tree)
  "Check that TREE is in CNF."
  (flet ((and-form (f)
	   (and (consp f)
		(eq :AND (car f))))
	 (or-form (f)
	   (and (consp f)
		(eq :OR (car f))))
	 (atomic (f)
	   (if (consp f)
	       (and (not (eq :OR (car f)))
		    (not (eq :AND (car f))))
	       T)))
    (if (atomic tree)
	T
	(and (and-form tree)
	     (every #'(lambda (c)
			(or (atomic c)
			    (and (or-form c)
				 (every #'atomic (cdr c)))))
		    (cdr tree))))))

(defun distribute (tree)
  "Pull out :ANDs to reduce standardized tree in NNF to CNF."
  (if (consp tree)
      (case (car tree)
	(:AND (let ((sub-cnfs (mapcar #'distribute (cdr tree))))
		(assert (every #'is-cnf sub-cnfs) 
			(sub-cnfs)
			"Not all in CNF: ~{~A~^~%~}" sub-cnfs)
		(let (clauses)
		  (loop :for sub-cnf :in sub-cnfs
		     :if (and (consp sub-cnf)
			      (eq :AND (car sub-cnf)))
		     ;; AND(AND  ..) = AND ..
		     :do (dolist (clause (cdr sub-cnf))
			   (push clause clauses)) 
		     :else :do (push sub-cnf clauses))
		  `(:AND ,@clauses))))
	(:OR (let (ands)
	       (dolist (term (mapcar #'distribute (cdr tree)))
		 (assert (is-cnf term) 
			 (term)
			 "~A not in CNF" term)
		 (if (consp term)
		     (case (car term)
		       (:AND (push (cdr term) ands))
		       (:OR (dolist (c (cdr term))
			      (push `(,c) ands)))
		       (otherwise (push `(,term) ands)))
		     (push `(,term) ands)))
	       (let ((clauses (crossprod ands)))
		 `(:AND ,@(mapcar #'(lambda (c)
				      (if (not (consp c))
					  c
					  `(:OR ,@ (loop :for subterm :in c
						      :if (and (consp subterm)
							       (eq :OR (car subterm)))
						      ;; (OR(OR...)) == (OR ...)
						      :append (cdr subterm)
						      ;; if C is not an OR-clause
						      ;; it is considered atomic:
						      :else :append `(,subterm)))))
				  clauses)))))
	(otherwise
	 tree))
      tree))


(defun tree->cnf (tree &optional (mapping #'cl:identity) (atom-test #'eql))
  "Convert TREE to CNF,  applying MAPPING to atoms and afterwards and removing duplicate literals in clauses using atom-test to test for equality. Also simplifies trivially fulfilled clauses to empty clause."
  (labels ((atomic (f)
	     (if (consp f)
		 (and (not (eq :OR (car f)))
		      (not (eq :AND (car f))))
		 T))
	   (map-literals (cnf)
	     ;; destructively apply MAPPING
	     (if (atomic cnf)
		 (setf cnf (funcall mapping cnf))
		 (setf (cdr cnf) ;; we must be looking at an AND
		       (mapcar #'(lambda (clause)
				   (if (atomic clause)
				       (funcall mapping clause)
				       `(:OR ,@(mapcar #'(lambda (lit)
							   (funcall mapping lit))
						       (cdr clause)))))
			       (cdr cnf))))
	     cnf)
	   (negated-litp (lit)
	     (and (consp lit) (eq :NOT (car lit))))
	   (atom-for (lit)
	     (if (consp lit)
		 (case (car lit)
		   (:NOT (atom-for (second lit)))
		   (:ATOM (second lit))
		   (otherwise lit))
		 lit))
	   (duplicate-literalsp (l1 l2)
	     (funcall atom-test (atom-for l1) (atom-for l2)))
	   (trivial-clausep (c)
	     (loop :for (lit . haystack) :on c
		:as needle := (atom-for lit)
		:as negated := (and (consp lit) (eq :NOT (car lit)))
		:when (find-if #'(lambda (x) 
				   (and (if negated
					    (not (negated-litp x))
					    (negated-litp x))
					(funcall atom-test needle (atom-for x))))
			       haystack)
		:return T
		:finally (return NIL)))
	   (clean-clauses (cnf)
	     ;; destructively apply MAPPING
	     (if (atomic cnf)
		 (setf cnf cnf)
		 (setf (cdr cnf) ;; we must be looking at an AND
		       (loop :for clause :in (cdr cnf)
			  :if (atomic clause)
			  :collect clause
			  :else :if (not (trivial-clausep clause))
			  :collect `(:OR ,@(remove-duplicates 
					    (cdr clause) 
					    :test #'duplicate-literalsp)))))
	     cnf))
    (clean-clauses
     (map-literals
      (distribute
       (standard-tree->nnf
	(standardize tree)))))))


(defgeneric add-formula (solver formula &key mapping atom-test)
  (:documentation "Add logical FORMULA to SOLVER.
Formula is an expression tree that can contain high-level abbreviations :IMPLY, :IFF, :NOR and :XOR, as well as the basic :AND, :OR, :NOT. Everything else is considered an atom. Atoms can also be explicitly written as (:ATOM foo). 
All atoms will be mapped through MAPPING before being added to the solver to convert your favorite atoms to phased variables. MAPPING is a function of 1 argument, the atom. It defaults to  #'identity. Afterwards, clean clauses (removing duplicates and simplifying trivial clauses, comparing atoms using ATOM-TEST (default: #'eql).")
  (:method ((solver sat-solver) (formula list) &key (mapping #'identity) (atom-test #'eql))
    (dolist (c (cdr ;; drop outer :AND
		(tree->cnf formula mapping atom-test)))
      (add-clause solver (cdr ;; drop :OR
			  c)))))

(defgeneric add-and (solver literals &key mapping)
  (:documentation "Add clauses for AND(literals) to SOLVER.")
  (:method ((solver sat-solver) (literals list) &key (mapping #'identity))
    (add-formula solver `(:AND ,@literals) mapping)))

(defgeneric add-or (solver literals &key mapping)
  (:documentation "Add clauses for OR(literals) to SOLVER.")
  (:method ((solver sat-solver) (literals list) &key (mapping #'identity))
    (add-formula solver `(:OR ,@literals) mapping)))

(defgeneric add-xor (solver literals &key mapping)
  (:documentation "Add clauses for binary XOR(literals) to SOLVER.")
  (:method ((solver sat-solver) (literals list) &key (mapping #'identity))
    (add-formula solver `(:XOR ,@literals) mapping)))

(defgeneric add-if (solver lhs rhs &key mapping)
  (:documentation "Add clauses for lhs -> rhs to SOLVER.")
  (:method ((solver sat-solver) (lhs list) (rhs list) &key (mapping #'identity))
    (add-formula solver `(:IMPLIES ,lhs ,rhs) mapping)))

(defgeneric add-iff (solver lhs rhs &key mapping)
  (:documentation "Add clauses for lhs <-> rhs to SOLVER.")
  (:method ((solver sat-solver) (lhs list) (rhs list) &key (mapping #'identity))
    (add-formula solver `(:IFF ,lhs ,rhs) mapping)))