;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; package.lisp --- SATWRAP package definitons

;; Copyright (C) 2010 Utz-Uwe Haus <lisp@uuhaus.de>
;;
;; $Id:$
;;
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 3 of the GNU General
;; Public License as published by the Free Software Foundation, as
;; clarified by the prequel found in LICENSE.Lisp-GPL-Preface.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 3 of the GNU General Public License is in the file
;; LICENSE.GPL that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/gpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; 

;;; Code:


(defpackage #:satwrap.backend
  (:use #:cl)
  ;; required interface for each backend:
  (:export #:satwrap-backend
	   #:numvars
	   #:add-clause
	   #:satisfiablep
	   #:solution)
  ;; optional interface:
  (:export #:get-essential-variables
	   #:synchronize-backend)
  ;; utilities
  (:export #:sat-backend-name #:sat-backend-version)
  )


(defpackage #:satwrap
  (:use #:CL)
  ;; class:
  (:export #:sat-solver)
  ;; ctor:
  (:export #:make-sat-solver)
  ;; accessors:
  (:export #:sat-solver-backend 
	   #:sat-solver-numvars
	   #:sat-solver-numclauses)
  ;; conditions:
  (:export #:satwrap-condition #:invalid-clause)
  ;; interface:
  (:export #:add-variable #:add-clause #:satisfiablep #:solution
	   #:get-essential-variables
	   #:with-sat-solver #:with-index-hash
	   #:add-clauses)
  ;; convenience functions for common logical operations; could be
  ;; handled specially one day by backends that support structure informations
  ;; on CNFs
  (:export #:add-formula)
  (:export #:add-and #:add-or #:add-xor #:add-if #:add-iff)
  ;; input/output
  (:export #:read-dimacs #:write-dimacs)
  ;; utils
  (:export #:describe-supported-backends
	   #:*default-sat-backend*
)
  )

