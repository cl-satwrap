#include "precosat_wrap.h"

#define ABS(x) 	((x)<0 ? -(x) : (x))
#define SIGN(x) ((x)<0 ? 1 : 0)
#define var2lit(v) ((2 * ABS(v)) + SIGN(v))

int
main()
{

	struct	precosat_solver *s = alloc_precosat_solver(10);

	precosat_add_var_to_clause(s,var2lit(1));
	precosat_add_var_to_clause(s,var2lit(-3));
	precosat_add_var_to_clause(s,var2lit(0));

	precosat_add_var_to_clause(s,var2lit(2));
	precosat_add_var_to_clause(s,var2lit(3));
	precosat_add_var_to_clause(s,var2lit(-1));
	precosat_add_var_to_clause(s,var2lit(0));

	precosat_solve(s);

	precosat_sol_val(s,var2lit(1));
	precosat_sol_val(s,var2lit(2));
	precosat_sol_val(s,var2lit(3));

	free_precosat_solver(s);
	return 0;
}
