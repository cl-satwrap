/* C wrapper code for precosat */

#include "precosat_wrap.h"
#include "precosat.hh"

extern "C" {

struct precosat_solver {
  PrecoSat::Solver * solver;
};

static void
init_precosat_solver(struct precosat_solver *s)
{
  /* default initialization, similar to what precosatmain.cc does */
/*   s->solver->set("verbose", 2); */
/*   s->solver->set("print", 1); */
  return;
};

struct precosat_solver *
alloc_precosat_solver(int numvars)
{
  struct precosat_solver *res = (struct precosat_solver*) malloc(sizeof(struct precosat_solver));
  if(res) {
    res->solver=new PrecoSat::Solver();
    res->solver->init(numvars);
    init_precosat_solver(res);
  };
  return res;
}

void
free_precosat_solver(struct precosat_solver *s)
{
  s->solver->reset(); /* de-allocation not done in destructor...*/
  delete s->solver;
  
  free(s);
}

int
precosat_set_option(struct precosat_solver *s, const char* optionname, int val)
{
  return s->solver->set(optionname,val);
}

enum precosat_result
precosat_solve(struct precosat_solver *s)
{
  int res;
  s->solver->fxopts();

  res=s->solver->solve(INT_MAX);

  if(res>0 && s->solver->satisfied())
    return PRECOSAT_SAT;
  else if (res<0)
    return PRECOSAT_UNSAT;
  else
    return PRECOSAT_ERROR;
}

int
precosat_sol_val(struct precosat_solver *s, int varidx)
{
  return s->solver->val(varidx);
}

void
precosat_add_var_to_clause(struct precosat_solver *s, int varid)
{
  s->solver->add(varid);
}

void
precosat_set_numvars(struct precosat_solver *s, int numvars)
{
  while (numvars > s->solver->getMaxVar())
    s->solver->next();
}

} /* extern "C" */

