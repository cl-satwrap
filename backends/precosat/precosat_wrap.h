/* C-wrapping for swig purposes */
/* (c) Utz-Uwe Haus 2010 */
#ifndef PRECOSAT_WRAP_H
#define PRECOSAT_WRAP_H

#include "precosat.hh"

extern "C" {
struct precosat_solver;

struct precosat_solver*
alloc_precosat_solver(int numvars);

void
free_precosat_solver(struct precosat_solver*);

int
precosat_set_option(struct precosat_solver *s, const char* optionname, int val);

enum precosat_result {
  PRECOSAT_SAT,
  PRECOSAT_UNSAT,
  PRECOSAT_ERROR
};

enum precosat_result
precosat_solve(struct precosat_solver*);

  int
  precosat_sol_val(struct precosat_solver *s, int varidx);
  

void
precosat_add_var_to_clause(struct precosat_solver *s, int varid);

void
precosat_set_numvars(struct precosat_solver *s, int numvars);

} /* extern "C" */

#endif
