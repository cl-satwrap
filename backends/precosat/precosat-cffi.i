/* swig interface wrapper file for cffi binding generation -*- lisp -*- */
/* swig -cffi interface for precosat */
/* (c) 2010 Utz-Uwe Haus */

%module "satwrap.precosat"

%feature("intern_function","1");   // use swig-lispify
%feature("export");                // export wrapped things

%insert("lisphead")%{
;;; Auto-generated -*- lisp -*- file
;;; generated from $Id:$
(eval-when (:compile-toplevel :load-toplevel)
  (declaim (optimize (speed 3) (debug 0) (safety 1))))

;;; Hiding swig:
(cl:defpackage :swig-macros
  (:use :cl :cffi)
  (:documentation
   "Package containing utility functions for SWIG cffi interface generation")
  (:export #:swig-lispify #:defanonenum))

(cl:defpackage :satwrap.precosat
  (:use :cl :cffi :swig-macros :satwrap.backend)
  (:documentation "Package containing precosat-backend")
  ;; other exports done by swig
  (:export #:precosat-backend))



(cl:in-package :swig-macros)

(cl:defun swig-lispify (name flag cl:&optional (package cl:*package*))
  (cl:labels ((helper (lst last rest cl:&aux (c (cl:car lst)))
		(cl:cond
		  ((cl:null lst)
		   rest)
		  ((cl:upper-case-p c)
		   (helper (cl:cdr lst) 'upper
			   (cl:case last
			     ((lower digit) (cl:list* c #\- rest))
			     (cl:t (cl:cons c rest)))))
		  ((cl:lower-case-p c)
		   (helper (cl:cdr lst) 'lower (cl:cons (cl:char-upcase c) rest)))
		  ((cl:digit-char-p c)
		   (helper (cl:cdr lst) 'digit 
			   (cl:case last
			     ((upper lower) (cl:list* c #\- rest))
			     (cl:t (cl:cons c rest)))))
		  ((cl:char-equal c #\_)
		   (helper (cl:cdr lst) '_ (cl:cons #\- rest)))
		  ((cl:char-equal c #\-)
		   (helper (cl:cdr lst) '- (cl:cons #\- rest)))
		  (cl:t
		   (cl:error "Invalid character: ~A" c)))))
    (cl:let ((fix (cl:case flag
		    ((constant enumvalue) "+")
		    (variable "*")
		    (cl:t ""))))
      (cl:intern
       (cl:concatenate
	'cl:string
	fix
	(cl:nreverse (helper (cl:concatenate 'cl:list name) cl:nil cl:nil))
	fix)
       package))))


%}

%{

/* includes that SWIG needs to see to parse the precosat.h file go here */

%}

%insert ("swiglisp") %{

(cl:in-package :satwrap.precosat)
%}

%insert ("swiglisp") %{
;; foreign type definitions to hide pointer values
(defclass wrapped-pointer ()
  ((ptrval :reader get-ptrval :initarg :ptrval)
   (ctype  :reader get-ctype :initarg :ctype))
  (:documentation "A wrapped pointer"))

(defmethod print-object ((p wrapped-pointer) stream)
  (print-unreadable-object (p stream :type nil :identity nil)
    (format stream "wrapper around `~A' @0x~16R" (get-ctype p) (get-ptrval p))))

(define-condition foreign-null-pointer-condition (error)
  ((type :initarg :type
         :reader foreign-null-pointer-condition-type))
  (:report (lambda (condition stream)
             (format stream "The foreign pointer of type ~A was NULL"
                     (foreign-null-pointer-condition-type condition)))))

%}

%define TYPEMAP_WRAPPED_POINTER(PTRTYPE,LISPCLASS)

%insert ("swiglisp") %{
(define-foreign-type LISPCLASS ()
  ()
  (:actual-type :pointer)
  (:documentation "cffi wrapper type around PTRTYPE."))

(define-parse-method LISPCLASS ()
  (make-instance 'LISPCLASS))

(defmethod translate-from-foreign (value (type LISPCLASS))
  "Wrap PTRTYPE, signaling a null-pointer-exception if value is NULL."
  (if (cffi:null-pointer-p value)
      (error 'foreign-null-pointer-condition :type type)
      (make-instance 'wrapped-pointer :ptrval value :ctype "PTRTYPE")))

(defmethod translate-to-foreign ((g wrapped-pointer) (type LISPCLASS))
  "Unwrap PTRTYPE"
  (get-ptrval g))

%}
%typemap (cin) PTRTYPE "LISPCLASS"
%typemap (cout) PTRTYPE "LISPCLASS"
%enddef

TYPEMAP_WRAPPED_POINTER(struct precosat_solver *, precosat-solver)

%typemap (cin)  struct precosat_solver * "precosat-solver"
%typemap (cout) struct precosat_solver * "precosat-solver"

/* Now parse and wrap the API header */
%insert ("swiglisp") %{
(eval-when (:compile-toplevel :load-toplevel)
  ;; Muffle compiler-notes globally
  #+sbcl (declaim (sb-ext:muffle-conditions sb-ext:defconstant-uneql))
  
%}

%include "precosat_wrap.h"
%insert ("swiglisp") %{

;; implementation of a satwrap backend using precosat:
(defclass precosat-backend (satwrap-backend)
  ((solver :initarg :solver :accessor precosat-backend-solver)
   (solved :initform NIL :accessor precosat-backend-solvedp))
  
  (:default-initargs :name "PrecoSat" :version "465r2"
		     :solver (alloc-precosat-solver 0))
  (:documentation "PrecoSat solver backend"))
) ;; end of eval-when to avoid top-level export 

(defmethod initialize-instance :after ((instance precosat-backend) &rest initargs)
  (let ((solver (precosat-backend-solver instance)))
    (tg:finalize instance #'(lambda ()
			      ;; (format T "Finalizing ~X." solver)
			      (free-precosat-solver solver)))))

(defmethod reinitialize-instance ((instance precosat-backend) &rest initargs &key &allow-other-keys)
  (apply #'call-next-method initargs )
  ;; fresh solver:
  (let ((new-solver (alloc-precosat-solver 0)))
    (setf (precosat-backend-solver instance) new-solver
	  (precosat-backend-solvedp instance) NIL)
    (tg:finalize instance #'(lambda () 
			      ;; (format T "Finalizing ~X." new-solver)
			      (free-precosat-solver new-solver))))  
  instance)



(defmacro varidx->literal (varidx)
  "Convert integral varidx where sign determines literal phase to 
the precosat form of nonnegative integral values with even indices for positive,
odd indices for negative phased literals."
  (let ((v (gensym "varidx")))
    `(let ((,v ,varidx))
	(declare (type fixnum ,v)
		 (optimize (speed 3) (safety 0))
		 #+allegro
		 (:explain :types :boxing :variables :tailmerging :inlining))
	(+ (* 2 (abs ,v))
	   (if (minusp ,v)
	       1
	       0)))))

(defmethod (setf numvars) ((numvars fixnum) (backend precosat-backend)) ;; arguments reversed for setf methods...
  (precosat-set-numvars (precosat-backend-solver backend) numvars))

(defmethod add-clause ((backend precosat-backend) (clause list))
  (assert (not (member 0 clause :test #'=)))
  (if (precosat-backend-solvedp backend)
      (error "Can not add clauses after solving.")
      (loop :with solver := (precosat-backend-solver backend)
	 :for v :of-type fixnum :in clause
	 :do (precosat-add-var-to-clause solver (varidx->literal v))
	 :finally (precosat-add-var-to-clause solver 0))))

(defmethod satisfiablep ((backend precosat-backend) (assumptions list))
  (if (precosat-backend-solvedp backend)
      (error "Can only solve once.")
      (progn
	(when assumptions
	  (dolist (c assumptions)
	    (add-clause backend `(,c))))
	(unwind-protect 
	     (ecase (precosat-solve (precosat-backend-solver backend))
	       (:PRECOSAT-SAT T)
	       (:PRECOSAT-UNSAT NIL)
	       (:PRECOSAT-ERROR (error "PrecoSat solver failed.")))
	  (setf (precosat-backend-solvedp backend) T)))))

(defmethod solution ((backend precosat-backend) (interesting-vars list))
  (if (not (precosat-backend-solvedp backend))
      (error "Problem still unsolved")
      (loop :with be := (precosat-backend-solver backend)
	 :for i :of-type fixnum :in interesting-vars
	 :collect (precosat-sol-val be (varidx->literal i)))))
%}


