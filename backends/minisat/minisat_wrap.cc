#include "minisat_wrap.h"

/* variables are indexed from 0 in minisat, but we like 1-based indexing. */

#define LISPVAR2MINISAT(x) ((x-1))
#define MINISATVAR2LISP(x) ((x+1))


extern "C" {
struct minisat_solver {
  SimpSolver *solver;
  vec<Lit> *incumbent_clause;
  vec<Lit> *assumptions;
};

static void
init_minisat_solver(struct minisat_solver *s)
{
  return;
};

struct minisat_solver *
alloc_minisat_solver()
{
  struct minisat_solver *res = (struct minisat_solver*) malloc(sizeof(struct minisat_solver));
  if(res) {
    res->solver=new SimpSolver();
    res->incumbent_clause=new vec<Lit>();
    res->assumptions=new vec<Lit>();
  };
  return res;
}

void
free_minisat_solver(struct minisat_solver *s)
{
  if(s->solver)
    delete s->solver;
  if(s->incumbent_clause)
    delete s->incumbent_clause;
  if(s->assumptions)
    delete s->assumptions;
  
  free(s);
}

int
minisat_set_option(struct minisat_solver *s, const char* optionname, int val)
{
  return -1;
}

enum minisat_result
minisat_solve(struct minisat_solver *s)
{
  int res;

  /* solve */
  res=s->solver->solve(*(s->assumptions)); 
     /* res=s->solver->solve(s->assumptions); */

  if(res)
    return MINISAT_SAT;
  else
    return MINISAT_UNSAT;
}

int
minisat_sol_val(struct minisat_solver *s, int varidx)
{
  lbool val = s->solver->model[LISPVAR2MINISAT(varidx)];

  if(val==l_True)
    return 1;
  else if(val==l_False)
    return 0;
  else
    return -1;
}

void
minisat_add_var_to_clause(struct minisat_solver *s, int varid)
{
  if(varid==0) {
    s->solver->addClause(*(s->incumbent_clause));
    s->incumbent_clause->clear();
  } else {
    int var = abs(varid) - 1;

    while(var >= s->solver->nVars())
      s->solver->newVar();
    
//     assert(var < s->solver->nVars());
    
    
    s->incumbent_clause->push(varid>0 ? Lit(var) : ~Lit(var));
  }
}

void
minisat_set_numvars(struct minisat_solver *s, int numvars)
{
  return;
  // minisat_add_clause does automatic resizing
  while (numvars > s->solver->nVars())
    s->solver->newVar();
}

void
minisat_clear_assumptions(struct minisat_solver *s)
{
  s->assumptions->clear();
}

  /* returns current number of assumptions after adding VARID */
int
minisat_add_assumption(struct minisat_solver *s, int varid)
{
  
  int var = abs(varid) - 1;
  
  s->assumptions->push(varid>0 ? Lit(var) : ~Lit(var));
  return s->assumptions->size();

}
} /* extern "C" */
