/* Minimalistic header API to minisat library */
/* (c) Utz-Uwe Haus 2010 */
#ifndef MINISAT_WRAP_H
#define MINISAT_WRAP_H

#include "simp/SimpSolver.h"

extern "C" {
struct minisat_solver;

struct minisat_solver*
alloc_minisat_solver();

void
free_minisat_solver(struct minisat_solver *s);

enum minisat_result {
  MINISAT_SAT,
  MINISAT_UNSAT,
  MINISAT_ERROR
};

enum minisat_result
minisat_solve(struct minisat_solver*);

int
minisat_sol_val(struct minisat_solver *s, int varidx);
  

void
minisat_add_var_to_clause(struct minisat_solver *s, int varid);

void
minisat_clear_assumptions(struct minisat_solver *s);

int
minisat_add_assumption(struct minisat_solver *s, int varid);

void
minisat_set_numvars(struct minisat_solver *s, int numvars);

} /* extern "C" */
#endif
