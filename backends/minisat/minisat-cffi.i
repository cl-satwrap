/* SWIG wrapper generation for minisat library -*- lisp -*- */
/* (c) 2010 Utz-Uwe Haus */

%module "satwrap.minisat"

%feature("intern_function","1");   // use swig-lispify
%feature("export");                // export wrapped things

%insert("lisphead")%{
;;; Auto-generated -*- lisp -*- file
;;; generated from $Id:$
(eval-when (:compile-toplevel :load-toplevel)
  (declaim (optimize (speed 3) (debug 0) (safety 1))))

(cl:defpackage :satwrap.minisat
  (:use :cl :cffi :satwrap.backend)
  (:documentation "Package containing minisat-backend")
  ;; other exports done by swig
  (:export #:minisat-backend #:satisiablep #:solution #:add-clause #:numvars))
(cl:in-package :satwrap.minisat)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (cl:defun swig-lispify (name flag cl:&optional (package cl:*package*))
    (cl:labels ((helper (lst last rest cl:&aux (c (cl:car lst)))
		  (cl:cond
		    ((cl:null lst)
		     rest)
		    ((cl:upper-case-p c)
		     (helper (cl:cdr lst) 'upper
			     (cl:case last
			       ((lower digit) (cl:list* c #\- rest))
			       (cl:t (cl:cons c rest)))))
		    ((cl:lower-case-p c)
		     (helper (cl:cdr lst) 'lower (cl:cons (cl:char-upcase c) rest)))
		    ((cl:digit-char-p c)
		     (helper (cl:cdr lst) 'digit 
			     (cl:case last
			       ((upper lower) (cl:list* c #\- rest))
			       (cl:t (cl:cons c rest)))))
		    ((cl:char-equal c #\_)
		     (helper (cl:cdr lst) '_ (cl:cons #\- rest)))
		    ((cl:char-equal c #\-)
		     (helper (cl:cdr lst) '- (cl:cons #\- rest)))
		    (cl:t
		     (cl:error "Invalid character: ~A" c)))))
      (cl:let ((fix (cl:case flag
		      ((constant enumvalue) "+")
		      (variable "*")
		      (cl:t ""))))
	(cl:intern
	 (cl:concatenate
	  'cl:string
	  fix
	  (cl:nreverse (helper (cl:concatenate 'cl:list name) cl:nil cl:nil))
	  fix)
	 package)))))

%}

%insert ("swiglisp") %{


%}

%insert ("swiglisp") %{
;; foreign type definitions to hide pointer values
(defclass wrapped-pointer ()
  ((ptrval :reader get-ptrval :initarg :ptrval)
   (ctype  :reader get-ctype :initarg :ctype))
  (:documentation "A wrapped pointer"))

(defmethod print-object ((p wrapped-pointer) stream)
  (print-unreadable-object (p stream :type nil :identity nil)
    (format stream "wrapper around `~A' @0x~16R" (get-ctype p) (get-ptrval p))))

(define-condition foreign-null-pointer-condition (error)
  ((type :initarg :type
         :reader foreign-null-pointer-condition-type))
  (:report (lambda (condition stream)
             (format stream "The foreign pointer of type ~A was NULL"
                     (foreign-null-pointer-condition-type condition)))))

%}

%define TYPEMAP_WRAPPED_POINTER(PTRTYPE,LISPCLASS)

%insert ("swiglisp") %{
(define-foreign-type LISPCLASS ()
  ()
  (:actual-type :pointer)
  (:documentation "cffi wrapper type around PTRTYPE."))

(define-parse-method LISPCLASS ()
  (make-instance 'LISPCLASS))

(defmethod translate-from-foreign (value (type LISPCLASS))
  "Wrap PTRTYPE, signaling a null-pointer-exception if value is NULL."
  (if (cffi:null-pointer-p value)
      (error 'foreign-null-pointer-condition :type type)
      (make-instance 'wrapped-pointer :ptrval value :ctype "PTRTYPE")))

(defmethod translate-to-foreign ((g wrapped-pointer) (type LISPCLASS))
  "Unwrap PTRTYPE"
  (get-ptrval g))

%}
%typemap (cin) PTRTYPE "LISPCLASS"
%typemap (cout) PTRTYPE "LISPCLASS"
%enddef

TYPEMAP_WRAPPED_POINTER(struct minisat_solver *, minisat-solver)

%typemap (cin)  struct minisat_solver * "minisat-solver"
%typemap (cout) struct minisat_solver * "minisat-solver"

/* Now parse and wrap the API header */
%insert ("swiglisp") %{
(eval-when (:compile-toplevel :load-toplevel)
  ;; Muffle compiler-notes globally
  #+sbcl (declaim (sb-ext:muffle-conditions sb-ext:defconstant-uneql))
  
%}

%include "minisat_wrap.h"
%insert ("swiglisp") %{

;; implementation of a satwrap backend using minisat:
(defclass minisat-backend (satwrap-backend)
  ((solver :initarg :solver :accessor minisat-backend-solver)
   (solved :initform NIL :accessor minisat-backend-solvedp))
  
  (:default-initargs :name "MiniSAT" :version "2-070721"
		     :solver (alloc-minisat-solver))
  (:documentation "MiniSAT solver backend"))
) ;; end of eval-when to avoid top-level export 

(defmethod initialize-instance :after ((instance minisat-backend) &rest initargs)
  (let ((solver (minisat-backend-solver instance)))
    (tg:finalize instance #'(lambda ()
			      ;; (format T "Finalizing ~X." solver)
			      (free-minisat-solver solver)))))

;; (defmethod reinitialize-instance ((instance minisat-backend) &rest initargs &key &allow-other-keys)
;;   (apply #'call-next-method initargs))



(defmethod (setf numvars) ((numvars fixnum) (backend minisat-backend)) ;; arguments reversed for setf methods...
  (minisat-set-numvars (minisat-backend-solver backend) numvars))

(defmethod add-clause ((backend minisat-backend) (clause list))
  (assert (not (member 0 clause :test #'=)))

  (loop :with solver := (minisat-backend-solver backend)
     :for v :of-type fixnum :in clause
     :do (minisat-add-var-to-clause solver v)
     :finally (progn
		(minisat-add-var-to-clause solver 0)
		(setf (minisat-backend-solvedp backend) NIL))))

(defmacro with-assumptions ((solver assumptions) &body body)
  (let ((i (gensym "i"))
	(s (gensym "solver"))
	(a (gensym "assumptions")))
    `(let ((,s ,solver)
	   (,a ,assumptions))
       (unwind-protect 
	    (progn
	      (minisat-clear-assumptions ,s)
	      (etypecase ,a
		(list (dolist (,i ,a)
			(minisat-add-assumption ,s ,i)))
		(vector (loop :for ,i :across ,a
			   :do (minisat-add-assumption ,s ,i))))
	      ,@body)
	 (minisat-clear-assumptions ,s)))))

(defmethod satisfiablep ((backend minisat-backend) (assumptions sequence))
  (ecase (let ((be (minisat-backend-solver backend)))
	   (with-assumptions (be assumptions)
	     (minisat-solve be)))
    (:+MINISAT-SAT+
     (setf (minisat-backend-solvedp backend) T)
     T)
    (:+MINISAT-UNSAT+ NIL)))

(defmethod solution ((backend minisat-backend) (interesting-vars list))
  (if (not (minisat-backend-solvedp backend))
      (error "Problem still unsolved or UNSAT")
      (loop :with be := (minisat-backend-solver backend)
	 :for i :of-type fixnum :in interesting-vars
	 :collect (minisat-sol-val be i))))

(defmethod synchronize-backend ((backend minisat-backend) 
				numvars
				(new-clauses list)
				(deleted-clauses (eql '()))
				(old-clauses list))
  "Adding clauses is easy with minisat."
  (declare (ignore numvars))
  (dolist (c new-clauses)
    (add-clause backend c)))
%}



